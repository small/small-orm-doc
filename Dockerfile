FROM python:3.8

RUN pip install --upgrade pip

RUN pip install mkdocs

RUN mkdir /usr/src/small-orm-docs
COPY src /usr/src/small-orm-docs

WORKDIR /usr/src/small-orm-docs

ENTRYPOINT sleep infinity
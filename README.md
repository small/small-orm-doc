# small-orm-doc

small-orm-doc is the documentation for small-orm project

You can read description of small-orm project bellow, or direct access [Documentation](src/index.md)

## What is small-orm ?

As small-orm is an ecosystem of packages serving a simple but efficient ORM for php developers.

It started from considering that lazy loading is not a good practice in terms of performance.

At each sub-object reached with lazy loading, a request is done, including network latency and time for database engine to build request.

small-orm allow you to load a group of objects in one time with joins, which is a heavy gain of performance for complex databases.

The around packages are oriented to api and microservice applications.

## Which databases engines supported ?

For now, Mysql and Redis are supported. It is planned to implement MongoDb.

## What about frameworks integration ?

small-orm is implemented in two frameworks :
* [Symfony](https://symfony.com)
* [Swoft](http://swoft.io) (small-orm swoft packages are not maintened anymore for swoft)

## Packages

### small-orm-core

This is the base package for small-orm. If your project is a non framework project or in a non implemented framework, use this package.

You can take it via :
* composer : https://packagist.org/packages/small/orm-core
* sources : https://framagit.org/small/small-orm-doc

### small-orm-bundle

This is a Symfony bundle to easy use small-orm in Symfony.

Before 2.x versions, it not needs small-orm-core : the core was originally developped in this bundle.

Since 2.x versions, the core have been extracted in order to open small-orm to other frameworks.

You can take it via :
* composer : https://packagist.org/packages/small/orm-bundle
* sources : https://framagit.org/small/small-orm-bundle

### small-user-bundle

This package implements user management in Symphony using small-orm. It provides api oriented routes to manage users.

Actually not migrated to 2.x versions.

You can take it via :
* composer : https://packagist.org/packages/sebk/small-user-bundle
* github : https://github.com/sebk69/SebkSmallUserBundle

### small-orm-swoole

This is a swoole connector for small-orm.

You can take it via :
* composer : https://packagist.org/packages/small/orm-swoole
* sources : https://framagit.org/small/small-orm-swoole

### small-orm-forms

This is a cross framework package. The goal of this package is to easily validate api calls and patch models.

You can take it via :
* composer : https://packagist.org/packages/small/orm-forms
* sources : https://framagit.org/small/small-orm-forms

### small-orm-swoft

**It is not maintained anymore.**

This is a Swoft package for small-orm.

This package come with specific connectors (MySql and Redis) in order to manage async requests.

You can take it via :
* composer : https://packagist.org/packages/sebk/small-orm-swoft
* github : https://github.com/sebk69/small-orm-swoft

### small-swoft-auth

**It is not maintained anymore.**

This is a simple jwt authentication for swoft that use small-orm.

It come with an abstract class to protect your controllers with near no code and use sebk/swoft-voter to implement your custom rights management.

You can take it via :
* composer : https://packagist.org/packages/sebk/small-swoft-auth
* github : https://github.com/sebk69/small-swoft-auth

### sebk/swoft-voter

**It is not maintained anymore.**

This is not really a small-om package but I mention it here because it is used by small-swoft-auth.

This package implement a voter system for Swoft, inspired by Symfony voters.

You can take it via :
* composer : https://packagist.org/packages/sebk/swoft-voter
* github : https://github.com/sebk69/swoft-voter
